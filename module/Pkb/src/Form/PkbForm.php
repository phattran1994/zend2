<?php
namespace Pkb\Form;

use Zend\Form\Form;

class PkbForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('pkb');

		$this->add([
            'name' => 'category',
            'type' => 'Zend\Form\Element\Select',
			'attributes' => array(
				'id' => 'category'
			),
			'options' => array(
				'label' => 'Category',
				'value_options' => array(
					'test' => 'testtext',
					'test2' => 'testtext2'
				),
			),
        ]);
        $this->add([
            'name' => 'id',
            'type' => 'text',
        ]);
        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
        ]);
        $this->add([
            'name' => 'description',
            'type' => 'text',
            'options' => [
                'label' => 'Description',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}