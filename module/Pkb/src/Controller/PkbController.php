<?php
namespace Pkb\Controller;

use Pkb\Model\PkbTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Pkb\Form\PkbForm;
use Pkb\Model\Pkb;

class PkbController extends AbstractActionController
{
	private $table;
	
	public function __construct(PkbTable $table)
    {
        $this->table = $table;
    }
	
    public function indexAction()
    {
		return new ViewModel([
            'Pkbs' => $this->table->fetchAll(),
        ]);
    }

    public function addAction()
    {
		$form = new PkbForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $pkb = new Pkb();
        $form->setInputFilter($pkb->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $pkb->exchangeArray($form->getData());
        $this->table->savePkb($pkb);
        return $this->redirect()->toRoute('pkb');
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
}