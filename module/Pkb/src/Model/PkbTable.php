<?php
namespace Pkb\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;

class PkbTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getPkb($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function savePkb(Pkb $Pkb)
    {
        $data = [
            'title'  => $Pkb->title,
			'description'  => $Pkb->description,
        ];

        $id = (int) $Pkb->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        if (! $this->getPkb($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update Pkb with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deletePkb($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}