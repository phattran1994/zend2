<?php
namespace Pkb;

use Zend\Router\Http\Segment;

return [
	'router' => [
        'routes' => [
            'pkb' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/pkb[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\PkbController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
	
    'view_manager' => [
        'template_path_stack' => [
            'pkb' => __DIR__ . '/../view',
        ],
    ],
];