CREATE TABLE categories (
id INTEGER PRIMARY KEY AUTOINCREMENT,
parent_id INTEGER REFERENCES categories(category_id),
name varchar(50) NOT NULL UNIQUE,
created_date datetime default CURRENT_DATE
);

INSERT INTO categories (name) VALUES ('Code');
INSERT INTO categories (parent_id, name) VALUES ((SELECT id FROM categories WHERE name = 'Code') , 'PHP');
INSERT INTO categories (parent_id, name) VALUES ((SELECT id FROM categories WHERE name = 'PHP'), 'ZF2');

CREATE TABLE records (
id INTEGER PRIMARY KEY AUTOINCREMENT,
category_id INTEGER REFERENCES categories(category_id) NOT NULL,
title varchar(100) NOT NULL, 
description varchar(200) NOT NULL, 
created_date datetime default CURRENT_DATE
);

INSERT INTO records (category_id, title, description) VALUES ((SELECT id FROM categories WHERE name = 'ZF2'), 'Test title 1', 'Test description of record 1');
INSERT INTO records (category_id, title, description) VALUES ((SELECT id FROM categories WHERE name = 'ZF2'), 'Test title 2', 'Test description of record 2');
INSERT INTO records (category_id, title, description) VALUES ((SELECT id FROM categories WHERE name = 'ZF2'), 'Test title 3', 'Test description of record 3');
INSERT INTO records (category_id, title, description) VALUES ((SELECT id FROM categories WHERE name = 'ZF2'), 'Test title 4', 'Test description of record 4');
INSERT INTO records (category_id, title, description) VALUES ((SELECT id FROM categories WHERE name = 'ZF2'), 'Test title 5', 'Test description of record 5');


